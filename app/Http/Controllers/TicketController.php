<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use App\Models\User;
use App\Models\Ticket;

class TicketController extends Controller
{
    public function save_apply_form(Request $data)
    {
        $this->validate(request(), 
        [
            'name' => 'required',
            'email' => 'required',
            'phone' => 'required',
            'description' => 'required',           
        ]);
    
    try {
        $ticket = new Ticket();

        $token = Str::random(10);

        $ticket->token = $token;
        $ticket->problem_description = $data['description'];
        $ticket->status = "pending";
       
        $ticket->user_id = auth()->user()->id;
        $ticket->save();

        $user =  User::where('id',auth()->user()->id)
                ->update([
                      "phone_no"=> $data['phone']
                ]);

             return redirect('/home');

        } catch (Exception $e) {
            return $this->responseServerError('Error getting resource.');
        }
    }


    public function supportList(Request $request)
    {
     
    try {
            $user = User::join('tickets','tickets.user_id','=','users.id')
                    ->select('users.id','users.name','users.email','users.phone_no','tickets.problem_description','tickets.reply','tickets.status')
                    ->where('users.id', '!=', '1')    
                    ->get();            
                   // ->paginate(10);

            if ($user) {
                return view('support_list', ['user' => $user]);
            } else if (!$user) {
                return redirect('/');
            }
        } catch (Exception $e) {
            return $this->responseServerError('Error getting resource.');
        }
    }

    public function viewReply(Request $request)
    {
     
    try {
        $id = $_GET["id"];

            $user = User::join('tickets','tickets.user_id','=','users.id')
                    ->select('users.id','users.name','users.email','users.phone_no','tickets.problem_description','tickets.reply','tickets.status')
                    ->where('users.id', '=', $id)
                    ->get();                
                    
            if ($user) {
                return view('reply_form', ['users' => $user]);
            } else if (!$user) {
                return redirect('/');
            }
        } catch (Exception $e) {
            return $this->responseServerError('Error getting resource.');
        }
    }

    public function updateReply(Request $request)
    {
        $this->validate(request(), 
        [
            'reply' => 'required',            
        ]);
    
        try{

            $id = $request['id'];

            $ticket = Ticket::where('user_id',$id)
            ->update([
                  "reply"=> $request['reply'],
                  "status"=> "done"
            ]);

         return redirect('/support-list');

        } catch (Exception $e) {
            return $this->responseServerError('Error getting resource.');
        }
    }

    public function viewAnswers(Request $request)
    {
     
    try {
        $id = auth()->user()->id;

            $user = User::join('tickets','tickets.user_id','=','users.id')
                    ->select('tickets.id','users.email','users.phone_no','tickets.problem_description','tickets.reply','tickets.status')
                    ->where('users.id', '=', $id)
                    ->get();
                    //->paginate(10);                
                    
            if ($user) {
                return view('cus_ans_form', ['answer' => $user]);
            } else if (!$user) {
                return redirect('/');
            }
        } catch (Exception $e) {
            return $this->responseServerError('Error getting resource.');
        }
    }

}

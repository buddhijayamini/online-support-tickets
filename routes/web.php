<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth/login');
});

Route::get('/email-view', function () {
    return view('email/myTestMail');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Auth::routes();

Route::get('/home', [\App\Http\Controllers\HomeController::class, 'index'])->name('home');


Route::post('/apply-form',[App\Http\Controllers\TicketController::class, 'save_apply_form']);

Route::get('/support-list', [App\Http\Controllers\TicketController::class, 'supportList']);

Route::get('/reply-form', [App\Http\Controllers\TicketController::class, 'viewReply']);

Route::get('/answer-list', [App\Http\Controllers\TicketController::class, 'viewAnswers']);

Route::post('/save-reply-form',[App\Http\Controllers\TicketController::class, 'updateReply']);

Route::get('send-mail', function () {
   
    $details = [
        'title' => 'Mail from Elegant Media Company',
        'body' => 'This is for testing email using smtp'
    ];
   
    \Mail::to('reciever_email@gmail.com')->send(new \App\Mail\MyTestMail($details));
   
    dd("Email is Sent.");
});
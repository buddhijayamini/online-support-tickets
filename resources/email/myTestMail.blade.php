@extends('layouts.app')
@section('content')

<main class="sm:container sm:mx-auto sm:mt-10">
    <div class="w-full sm:px-6">
    
    <h1>Elegant Media Company</h1>

    <h2>{{ $details['title'] }}</h2>
    <p>{{ $details['body'] }}</p>
   
    <p>Thank you</p>
       </div>
</main>
@endsection
 @if(Auth::user() == null)
      
         <script>window.location="/";</script>

@elseif(Auth::user()->id == 1)

    <!-- Custom styles for this template -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/1.11.1/css/dataTables.bootstrap4.min.css" rel="stylesheet">
    
@extends('layouts.app')
@section('content')

<main class="sm:container sm:mx-auto sm:mt-10">
    <div class="w-full sm:px-6">

        @if (session('status'))
            <div class="text-sm border border-t-8 rounded text-green-700 border-green-600 bg-green-100 px-3 py-4 mb-4" role="alert">
                {{ session('status') }}
            </div>
        @endif

        <section class="flex flex-col break-words bg-white sm:border-1 sm:rounded-md sm:shadow-sm sm:shadow-lg">

            <header class="font-semibold bg-gray-200 text-gray-700 py-5 px-6 sm:py-6 sm:px-8 sm:rounded-t-md">
                Support List
            </header>

          @if (session('error'))
                  <div class="alert alert-danger">{{ session('error') }}</div>
                 @endif

                    @if(count($errors))
                        <div class="form-group">
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach($errors->all() as $error)
                                        <li>{{$error}}</li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    @endif

            <div class="w-full p-6">
                <form class="w-full px-6 space-y-6 sm:px-10 sm:space-y-8" method="POST" action="{{ url('apply-form') }}">
                    @csrf

                <table class="table table-striped table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                    <tr > 
                            <th>id</th>
                            <th>Customer Name</th>
                            <th>Email</th>
                            <th>Phone No:</th>
                            <th>Problem Description</th>
                            <th>Reply for Problem</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody >
                          @foreach ($user as $us)
                                <tr >
                                    <td >{{ $us->id }}</td>
                                    <td >{{ $us->name }}</td>
                                    <td >{{ $us->email }}</td>
                                    <td >{{ $us->phone_no }}</td>
                                    <td >{{ $us->problem_description }}</td>
                                    <td >
                                      @if ($us->reply == null)
                                                 - 
                                    @else
                                         {{$us->reply}}
                                    @endif
                                    </td>
                                    <td >
                                    @if ($us->status == "pending")
                                         <label style = "color:red">{{ $us->status }}</label>
                                    @else
                                         {{ $us->status }}
                                    @endif
                                   </td>
                                    <td ><button type="button" class="btn btn-primary" ><a href="/reply-form?id={{ $us->id }}" style="color:white">View</a></button> </td>                                     
                                </tr>
                             @endforeach
                        </tbody>
                        </table>
                </form>

            </div>
        </section>
         <!-- Page level plugins -->
    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script src="https://cdn.datatables.net/1.11.1/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.11.1/js/dataTables.bootstrap4.min.js"></script>
    
      <script>
      $(document).ready(function() {
         $('#dataTable').DataTable();
        });
      </script>
   
    </div>
</main>
@endsection
@endif